<?php

function __autoload($className){
	$paths = array(
		"Controllers",
		"Helpers",
		"Models",
		"Models/Repositories"
	);
	foreach ($paths as $path){
		$path = $path."/".$className.".php";
		if (file_exists($path)) {
			require_once $path;
		}
	}
}