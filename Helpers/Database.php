	<?php

Class Database {

	private $databaseConnection;

	public function __construct(){

		try {
			$configuration = require "Configuration/Configuration.php";
			$username = $configuration["database"]["username"];
			$password = $configuration["database"]["password"];
			$hostname = $configuration["database"]["hostname"];
			$database = $configuration["database"]["database"];
		} catch (Exception $e) {
			die ($e);
		}

		$this->databaseConnection = mysqli_connect($hostname,$username,$password);
		if ($this->databaseConnection){
			$databaseResource = mysqli_select_db($this->databaseConnection, $database);
			if (!$databaseResource){
				die ("Access denied for database: '$database'");
			}
		} else {
			die ("Access denied for user: '$username' and password: '$password' on hostname: '$hostname'");
		}

	}

	public function executeQuery($query) {

		$result = mysqli_query($this->databaseConnection, $query);
		if ($result){
			return $result;
		} else {
			return NULL;
		}

	}

	public function lastID(){
		return mysqli_insert_id($this->databaseConnection);
	}

	public function __destruct(){
		mysqli_close($this->databaseConnection);
	}

}