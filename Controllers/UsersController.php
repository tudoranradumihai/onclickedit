<?php

Class UsersController extends Controller {
	
	public $usersRepository;

	public function __construct(){
		$this->usersRepository = new UsersRepository();
	}

	public function defaultAction(){
		self::authAction();
	}
	
	public function authAction(){
		require "Views/Users/auth.php";
	}

	public function connectAction(){
		if ($_SERVER["REQUEST_METHOD"]=="POST"){
			$result = $this->usersRepository->checkCredentials($_POST["emailaddress"],$_POST["password"]);
			if ($result){
				$_SESSION["user"] = $result;
				header("Location: index.php?C=Users&A=edit");
			} else {
				header("Location: index.php?C=Users&A=auth");
			}
		} else {
			header("Location: index.php");
		}

	}

	public function disconnectAction(){
		// logout
	}

	public function editAction(){
		
		require "Views/Users/edit.php";
	}

}